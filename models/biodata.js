"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Biodata.hasOne(models.User, { foreignKey: "biodata_id", as: "user" });
    }
  }
  Biodata.init(
    {
      name: DataTypes.STRING,
      path_avatar: DataTypes.STRING,
      about: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "Biodata",
    }
  );
  return Biodata;
};
