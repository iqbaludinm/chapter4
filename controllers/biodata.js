const { Biodata } = require("../models");

createBiodata = async (req, res) => {
  try {
    const { name, path_avatar, about } = req.body;
    const biodata = await Biodata.create({
      name,
      path_avatar,
      about,
    });

    res.status(201).json({
      status: "success",
      message: "Biodata created successfully",
      data: biodata,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

getAllBiodata = async (req, res) => {
  try {
    const biodata = await Biodata.findAll();

    res.status(200).json({
      status: "success",
      message: "Biodata was retrieved succesfully",
      data: biodata,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

getBiodata = async (req, res) => {
  try {
    const biodata_id = req.params.id;
    const biodata = await Biodata.findOne({
      where: { id: biodata_id },
    });

    if (!biodata) {
      res.status(404).json({
        status: "error",
        message: "can't find biodata with id " + biodata_id,
        data: null,
      });
      return;
    }

    res.status(200).json({
      status: "success",
      message: `success retrived biodata with id ${biodata_id}`,
      data: biodata,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

updateBiodata = async (req, res) => {
  try {
    const { name, path_avatar, about } = req.body;
    const biodata_id = req.params.id;
    let query = {
      where: { id: biodata_id },
    };

    const biodataIsExist = await Biodata.findOne({ where: { id: biodata_id } });

    if (!biodataIsExist) {
      res.status(401).json({
        status: "error",
        message: `biodata with id ${biodata_id} not found`,
        data: null,
      });
      return;
    }

    let updatedBiodata = await Biodata.update(
      {
        name,
        path_avatar,
        about,
      },
      query
    );

    res.status(200).json({
      status: "success",
      message: "success updated biodata with id " + biodata_id,
      data: updatedBiodata,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

deleteBiodata = async (req, res) => {
  try {
    const biodata_id = req.params.id;
    const biodataIsExist = await Biodata.findOne({ where: { id: biodata_id } });

    if (!biodataIsExist) {
      res.status(401).json({
        status: "error",
        message: `biodata with id ${biodata_id} not found`,
        data: null,
      });
      return;
    }

    const destroyBiodata = await Biodata.destroy({ where: { id: biodata_id } });
    res.status(200).json({
      status: "success",
      message: "deleted biodata succesfully",
      data: destroyBiodata,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

module.exports = {
  createBiodata,
  getAllBiodata,
  getBiodata,
  updateBiodata,
  deleteBiodata,
};
