const { User, Biodata } = require("../models");

createUser = async (req, res) => {
  try {
    let { username, password, biodata_id } = req.body;

    const biodataIsExist = await Biodata.findOne({ where: { id: biodata_id } });

    if (!biodataIsExist) {
      res.status(404).json({
        status: "error",
        message: `biodata with id ${biodata_id} is not found`,
        data: null,
      });
      return;
    }

    let newUser = await User.create({
      username,
      password,
      biodata_id,
    });

    res.status(201).json({
      status: "success",
      message: "successfully created user",
      data: newUser,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      error: err.message,
    });
  }
};

getUsers = async (req, res) => {
  try {
    let users = await User.findAll({
      include: ["biodata"],
    });

    res.status(200).json({
      status: "success",
      message: "successfully retrieve all data user",
      data: users,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      error: err,
    });
  }
};

getUserById = async (req, res) => {
  try {
    const user_id = req.params.id;
    let user = await User.findOne({
      where: { id: user_id },
      include: ["biodata"],
    });

    if (!user) {
      res.status(404).json({
        status: "error",
        message: "can't find user with id " + user_id,
        data: null,
      });
      return;
    }

    res.status(200).json({
      status: "success",
      message: "successfully retrieve data user with id " + user_id,
      data: user,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      error: err.message,
    });
  }
};

updateUser = async (req, res) => {
  try {
    let { username, password, biodata_id } = req.body;
    let { id } = req.params;
    let query = {
      where: {
        id,
      },
    };
    let updatedUser = await User.update(
      {
        username,
        password,
        biodata_id,
      },
      query
    );

    res.status(200).json({
      status: "success",
      message: "successfully updated data",
      data: updatedUser,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      error: err.message,
    });
  }
};

deleteUser = async (req, res) => {
  try {
    const user_id = req.params.id;
    let deleted = await User.destroy({
      where: {
        id: user_id,
      },
    });

    res.status(200).json({
      status: "success",
      message: "successfully deleted user with id " + user_id,
      data: deleted,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      error: err.message,
    });
  }
};

module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
};
