const express = require("express");
const morgan = require("morgan");
const app = express();
const port = 3000;
const router = require("./routes");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(morgan("dev"));
app.use(express.json());
app.use("/api/v1", router);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port, () => {
  console.log(`server running on ${port}`);
});
