const express = require("express");
const router = express.Router();

const user = require("./user");
const biodata = require("./biodata");
const history = require("./history");

router.get("/", (req, res) => {
  res.status(200).json({
    status: "success",
    data: "Welcome on Dashboard!",
  });
});

router.use("/user", user);
router.use("/biodata", biodata);
router.use("/history", history);

module.exports = router;
