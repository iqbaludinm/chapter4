const express = require("express");
const router = express.Router();
const {
  createHistory,
  getHistories,
  getHistory,
  updateHistory,
  deleteHistory,
} = require("../controllers/history");

router.post("/", createHistory);
router.get("/", getHistories);
router.get("/:id", getHistory);
router.put("/:id", updateHistory);
router.delete("/:id", deleteHistory);

module.exports = router;
