const express = require("express");
const router = express.Router();
const {
  createBiodata,
  getAllBiodata,
  getBiodata,
  updateBiodata,
  deleteBiodata,
} = require("../controllers/biodata");

router.post("/", createBiodata);
router.get("/", getAllBiodata);
router.get("/:id", getBiodata);
router.put("/:id", updateBiodata);
router.delete("/:id", deleteBiodata);

module.exports = router;
